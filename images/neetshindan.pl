#!/usr/bin/perl
use strict;
use CGI qw(:standard);
#use CGI::Carp qw(fatalsToBrowser);
#use Time::Piece;

require "lib/lib.pl";
require "lib/config.pl";
require "lib/neetcdn.pl";

my %cdn=&initcdn;

my %resource;
my $qCGI=new CGI;
my %form=&init_form($qCGI);
my $name_maxlen_mb=8;
my $name_maxlen_sp=10;
my $name_maxlen_pc=20;
my $lists_pc=10;
my $lists_mb=5;
my $more_lists_pc=50;
my $more_lists_mb=20;

&initconfig(%form);

&main;

sub main {
	my $err;
	binmode(STDOUT, ":utf8");

	%resource=&readresource($::resource);

	my $template;

	# 設問開始
	my $maxid=&searchsetsumonmaxid;

	my $seed=&makeseed(%form);

	# アンケート
	if($form{enq} ne "") {
		open(W, ">>result/enq.txt");
		my @enq;
		foreach(keys %form) {
			if(/^enq\_/) {
				push(@enq, "$_\n$form{$_}\n\n");
			}
		}
		@enq=sort @enq;
		print W join("\n", @enq);
		print W "----------------------\n";
		close(W);
		my $html=&readfile("template/thanks.html");
			$html=&replace($html,
				name=>$form{name},
				cdnbase=>$cdn{base},
				cdnimage=>$cdn{image},
				cdncss=>$cdn{css},
				cdnjs=>$cdn{js},
				cdnfont=>$cdn{font},
				script=>$::script);
		&output($html);
		exit;
	}
	# 強制転送
	if($form{loc} ne "") {
		my $query="qid=$form{qid}&qno=$form{qno}&name=@{[&query_encode($form{name})]}";

		for(my $i=1; $i <= $maxid; $i++) {
			my $forme="e$i";
			my $formv="v$i";
			if($form{$forme} ne "") {
				$query.="&$forme=$form{$forme}";
			}
			if($form{$formv} ne "") {
				$query.="&$formv=$form{$formv}";
			}

			foreach(keys %form) {
				if(/\|/) {
					my($n, $e, $v)=split(/\|/, $_);
					$n=~s/n//g;
					$query.="&e$n=$e&v$n=$v";
				}
			}

		}
		print <<EOM;
Location: $::script?$query&seed=$seed&device=$form{device}&lang=$form{lang}

EOM
		exit;
	# 開始時の転送
	} elsif($form{start} ne "") {
		if(&namechk($form{name})) {
			my @qids;
			for(my $i=1; $i<=$maxid; $i++) {
				push(@qids, $i);
			}
			&shuffleArray(\@qids, %form);

			# N E1 E2 T から６問ずつ抽出
			my @newqids;
			my %neet;
			foreach my $qid(@qids) {
				my %setsumon=&readsetsumon($qid);
				if($neet{$setsumon{val}}++ >= 6) {
					next;
				}
				push(@newqids, $qid);
			}
			@qids=@newqids;
			&shuffleArray(\@qids, %form);

			my $qids=join(',', @qids);

			print <<EOM;
Location: $::script?qid=$qids&qno=1&name=@{[&query_encode($form{name})]}&seed=$seed&device=$form{device}&lang=$form{lang}

EOM
			exit;
		} else {
			if($form{name} eq "") {
				$err=qq(<font color="red"><strong>$resource{noname}</strong></font>);
			} else {
				$err=qq(<font color="red"><strong>$resource{namestrerr}</strong></font>);
			}
		}
	# 質問中？
	} elsif($form{qid} ne "" && $form{qno} ne "") {
		# 質問終了
		my @setsumons=split(/,/,$form{qid});
		if($form{qno} > $#setsumons+1) {
			my ($body, $zokusei, $result, $type, $zarray, $h)=&calcneet;
			# 再度転送する
			if($form{result} eq "") {
				$form{result}=1;
#				$form{name}=&query_encode($form{name});
				my @z=@$zarray;
				&writecount($type, $#z, @z);
				&writeresult($zokusei, %$result);
				my $query;
				foreach(keys %form) {
					$query.=qq(&$_=@{[&query_encode($form{$_})]});
				}
				print <<EOM;
Location: $::script?a=a$query

EOM
				exit;
			}
			my $html=&readfile($::template_result);
			my @chara=split(/\\n/, $$result{chara});
			my $ctitle=shift @chara;
			my $ccomment=join("<br>\n", @chara);
			$html=&replace($html,
				name=>$form{name},
				device=>$form{device},
				lang=>$form{lang},
				zokusei=>$zokusei,
				charatitle=>$ctitle,
				characomment=>$ccomment,
				cate=>$$result{cate},
				kata=>$$result{kata},
				kuken=>$$result{kuken},
				ptn=>$$result{ptn},
				neet=>$$result{neet},
				jigyou=>$$result{jigyou},
				seed=>$form{seed},
				td=>$body,
				cdnbase=>$cdn{base},
				cdnimage=>$cdn{image},
				cdncss=>$cdn{css},
				cdnjs=>$cdn{js},
				cdnfont=>$cdn{font},
				p1=>$$h{N},
				p2=>$$h{E1},
				p3=>$$h{E2},
				p4=>$$h{T},
				script=>$::script);
			&output($html);
			exit;
		# 質問中
		} else {
			my @qids=split(/,/, $form{qid});
			my $qid=$qids[$form{qno}-1];
			my %setsumon=&readsetsumon($qid);

			my $hiddens=<<EOM;
<input type="hidden" name="qid" value="$form{qid}" />
<input type="hidden" name="qno" value="@{[$form{qno} + 1]}" />
EOM

			for(my $i=1; $i <= $maxid; $i++) {
				my $forme="e$i";
				my $formv="v$i";
				if($form{$forme} ne "") {
					$hiddens.=<<EOM;
<input type="hidden" name="$forme" value="$form{$forme}" />
EOM
				}
				if($form{$formv} ne "") {
					$hiddens.=<<EOM;
<input type="hidden" name="$formv" value="$form{$formv}" />
EOM
				}
			}
			foreach(keys %form) {
				if(/\|/) {
					my($n, $e, $v)=split(/\|/, $_);
					$n=~s/n//g;
					$hiddens.=<<EOM;
<input type="hidden" name="e$n" value="$e" />
<input type="hidden" name="v$n" value="$v" />
EOM
				}
			}
			my ($e1, $dmy, $e2)=split(/ or /, $setsumon{val});
			foreach("N", "E1", "E2", "T") {
				my $ans="ans$_";
				if($e1 eq $_) {
					$hiddens.=<<EOM;
<input type="hidden" name="$ans" value="@{[$form{$ans}+1]}" />
EOM
				} else {
					$hiddens.=<<EOM;
<input type="hidden" name="$ans" value="@{[$form{$ans}+0]}" />
EOM
				}
			}
			my $html=&readfile($::template_q);

			$html=&replace($html,
				qcount=>$resource{"q" . $form{qno}},
				cdnbase=>$cdn{base},
				cdnimage=>$cdn{image},
				cdncss=>$cdn{css},
				cdnjs=>$cdn{js},
				cdnfont=>$cdn{font},
				script=>$::script,
				name=>$form{name},
				device=>$form{device},
				lang=>$form{lang},
				seed=>$form{seed},
				qno=>$form{qno},
				qtext=>$setsumon{text},
				qline=>$setsumon{line},
				qval=>$e1,
				qn1=>$setsumon{n1},
				qv1=>$setsumon{v1},
				qn2=>$setsumon{n2},
				qv2=>$setsumon{v2},
				qn3=>$setsumon{n3},
				qv3=>$setsumon{v3},
				hiddens=>$hiddens
			);
			&output($html);
			exit;
		}
	# アーカイブ
	} elsif($form{ud} ne "" && $form{uc} ne "") {
		if(1) {
			my $fid=$form{ud};
			$fid="0";
			my $uid=$form{uc}-1;
			if(-r "$::dir{result}/result-$fid.txt") {
				my $buf=&readfile("$::dir{result}/result-$fid.txt");
				my @result=split(/\n/, $buf);
				my ($time, $ip, $neetid, $name, $ptn, $zokusei, $neet, $jigyou, $query)=split(/\t/, $result[$uid]);
				if ($query =~ /&/) {
					my @querys = split(/&/, $query);
					foreach (@querys) {
						$_ = &query_decode($_);
						$form{$1}=$2 if (/([^=]*)=(.*)$/);
					}
				}
				my $html=&readfile($::template_result);
#				my ($body, $zokusei, $result)=&calcneet;
				my ($body, $zokusei, $result, $type, $zarray, $h)=&calcneet;
				my @chara=split(/\\n/, $$result{chara});
				my $ctitle=shift @chara;
				my $ccomment=join("<br>\n", @chara);
				$html=&replace($html,
					name=>$form{name},
					device=>$form{device},
					lang=>$form{lang},
					zokusei=>$zokusei,
					charatitle=>$ctitle,
					characomment=>$ccomment,
					cate=>$$result{cate},
					kata=>$$result{kata},
					kuken=>$$result{kuken},
					ptn=>$$result{ptn},
					neet=>$$result{neet},
					jigyou=>$$result{jigyou},
					seed=>$form{seed},
					td=>$body,
					cdnbase=>$cdn{base},
					cdnimage=>$cdn{image},
					cdncss=>$cdn{css},
					cdnjs=>$cdn{js},
					cdnfont=>$cdn{font},
					p1=>$$h{N},
					p2=>$$h{E1},
					p3=>$$h{E2},
					p4=>$$h{T},
					script=>$::script);
				&output($html);
			}
		exit;
		}
	}
	# TOP
	my $html=&readfile($form{more} eq "" ? $::template_start : $::template_more);

	my $lists=
		$form{more} eq ""
			? ($::device eq "pc" ? $lists_pc : $lists_mb)
			: ($::device eq "pc" ? $more_lists_pc : $more_lists_mb);
	my @rlist=&resultlist(0, $lists);
	my $rlist;
	foreach(@rlist) {
		my($ud, $uc, $dt, $name, $ptn)=split(/\t/, $_);
		if($::device eq "mb") {
			my $_name=&strcutbytes_utf8($name, $name_maxlen_mb);
			$rlist.=<<EOM;
<a href="$::script?ud=$ud&uc=$uc&device=$::device&lang=$::lang">$dt $_name ($ptn)</a><br />
EOM
		} elsif($::device eq "sp") {
			my $_name=&strcutbytes_utf8($name, $name_maxlen_sp);
			$rlist.=<<EOM;
<tr>
<td><a href="$::script?ud=$ud&uc=$uc&device=$::device&lang=$::lang">$dt</a></td>
<td><a href="$::script?ud=$ud&uc=$uc&device=$::device&lang=$::lang">$_name</a></td>
<td><a href="$::script?ud=$ud&uc=$uc&device=$::device&lang=$::lang">($ptn)</a></td>
</tr>
EOM
		} else {
			my $_name=&strcutbytes_utf8($name, $name_maxlen_pc);
			$rlist.=<<EOM;
<tr>
<td><a href="$::script?ud=$ud&uc=$uc&device=$::device&lang=$::lang">$dt</a></td>
<td><a href="$::script?ud=$ud&uc=$uc&device=$::device&lang=$::lang">$_name</a></td>
<td><a href="$::script?ud=$ud&uc=$uc&device=$::device&lang=$::lang">($ptn)</a></td>
</tr>
EOM
		}
	}

	my %z=&readcount;
	my %zp;
	if($z{total}+0 ne 0) {
		foreach(keys %z) {
			$zp{$_}=sprintf("%.1f", $z{$_} / $z{total} * 100);
		}
	}
	my ($second, $minute, $hour, $mday, $month, $year)
		= localtime(time + 0 * 3600);

	my $dtstr=sprintf($resource{date}
		, $month+1, $mday, $hour, $minute);

	$html=&replace($html,
		err=>$err,
		name=>$form{name},
		list=>$rlist,
		date=>$dtstr,
		device=>$form{device},
		lang=>$form{lang},
		ztotal=>$z{total},
		zyamivar=>$z{yami},
		zfirevar=>$z{fire},
		zkurovar=>$z{kuro},
		zchivar=>$z{chi},
		zhikarivar=>$z{hikari},
		zmizuvar=>$z{mizu},
		zshirovar=>$z{shiro},
		zkazevar=>$z{kaze},
		zaaaavar=>$z{aaaa},
		zbbbbvar=>$z{bbbb},
		zccccvar=>$z{cccc},
		zyamiper=>$zp{yami},
		zfireper=>$zp{fire},
		zkuroper=>$zp{kuro},
		zchiper=>$zp{chi},
		zhikariper=>$zp{hikari},
		zmizuper=>$zp{mizu},
		zshiroper=>$zp{shiro},
		zkazeper=>$zp{kaze},
		zaaaaper=>$zp{aaaa},
		zbbbbper=>$zp{bbbb},
		zccccper=>$zp{cccc},
		cdnbase=>$cdn{base},
		cdnimage=>$cdn{image},
		cdncss=>$cdn{css},
		cdnjs=>$cdn{js},
		cdnfont=>$cdn{font},
		maxlists=>$lists,
		script=>$::script);
	&output($html);
}

sub output {
	my ($html)=shift;
	if($::device eq "mb") {
		require "lib/kana.pl";
		$html=&z2h($html);
	}
	print <<EOM;
Content-type: text/html; charset=utf-8

$html
EOM
}

sub namechk {
	my($s)=shift;

	return 0 if($s eq "");
	if($s=~/[!-\/:-@-`{-~<>;\[\]]\&\%/) {
		return 0;
	}
	return 1;
}

sub calcneet {
	my %h=&addneet;

	my $body;
	$body=<<EOM;
debug calc result:<br>
N:$h{N}<br>
E1:$h{E1}<br>
E2:$h{E2}<br>
T:$h{T}<br>
<hr>
EOM


	my($type, $zokusei, @zokusei)=&getzokusei(%h);

	my %result=&result($type);

if(0){
	$body.=<<EOM;
result:$type<br>
属性：$zokusei<br>
カテゴリー：$result{cate}<br>
型：$result{kata}<br>
典型・変形：$result{kuken}<br>
配列パターン：$result{ptn}<br>
考えられる属性(SR)：$result{sr}<br>
考えられる属性(HR)：$result{hr}<br>
考えられる属性(R)：$result{r}<br>
考えられる属性(N)：$result{n}<br>
EOM
}
	my $characomment=$result{chara};

	return($body, $zokusei, \%result, $type, \@zokusei, \%h);
}

sub addneet {
	my $maxid=&searchsetsumonmaxid;
	my %h;

	$h{N}=0;
	$h{E1}=0;
	$h{E2}=0;
	$h{T}=0;

	for(my $i=1; $i <= $maxid; $i++) {
		my $hkey=$form{"e" . $i};
		my $hval=$form{"v" . $i};
		$h{$hkey} += $hval;
	}
	%h;
}


sub getzokusei {
	my(%h)=@_;

	my (%zokuseistr, %zokusei)=&readzokusei;
	my $zokusei;

	my $type;
	my $tmp=0;
	foreach my $t("N", "E1", "E2", "T") {
		if($h{$t} > 0) {
			my $tt=$h{$t};
			if($tmp < $tt) {
				$tmp=$tt;
				$zokusei=$zokuseistr{uc $t} . " ";
			} elsif($tmp eq $tt) {
				$zokusei.=$zokuseistr{uc $t} . " ";
			}
			$type.="A";
		} elsif($h{$t} eq 0) {
			$type.="B";
		} else {
			my $tt=-$h{$t};
			if($tmp < $tt) {
				$tmp=$tt;
				$zokusei=$zokuseistr{lc $t} . " ";
			} elsif($tmp eq $tt) {
				$zokusei.=$zokuseistr{lc $t} . " ";
			}
			$type.="C";
		}
	}
	my @zokusei=split(/ /, $zokusei);
	my $zcnt=$#zokusei+1;
	$zcnt=5 if($type eq "AAAA" || $type eq "BBBB" || $type eq "CCCC");
	my $ztmp2=join($resource{conma}, @zokusei);
	my $ztmp=sprintf($resource{"zokusei" . $zcnt}, $ztmp2);
	return($type, $ztmp, @zokusei);
}

sub writeresult {
	my($zokusei, %result)=@_;
	my $ud=&udate;
	$ud="0";
	mkdir($::dir{result});
	open my $fh, ">>:utf8", "$::dir{result}/result-$ud.txt";
	print $fh time . "\t$ENV{REMOTE_ADDR}\t$ENV{REMOTE_USER}\t$form{name}\t$result{ptn}\t$zokusei\t$result{neet}\t$result{jigyou}\t$ENV{QUERY_STRING}\n";
	close($fh);
}

sub idrand {
	my $maxid=&searchsetsumonmaxid;
	my @array;
	for(my $i=1; $i <= $maxid; $i++) {
		push(@array, $i);
	}
	&shuffleArray(\@array, %form);
	@array;
}

sub searchsetsumonmaxid {
#	return 24;
	my @array=&readcsv($::setsumon);

	my $tmpid=0;

	foreach my $line(@array) {
		my($_line, $_id, $_val, $_text
			, $_n1, $_v1, $_n2, $_v2, $_n3, $_v3)
			= split(/\t/, $line);
		$tmpid=$_id if($tmpid < $_id);
	}
	$tmpid;
}

sub readzokusei {
	my @array=&readcsv($::zokusei);
	my %z;
	my %s;
	foreach my $line(@array) {
		my($_dmy, $_id, $_val, $_str)=split(/\t/, $line);
		$z{$_id}=$_val;
		$s{$_id}=$_str;
	}
	return(%z, %s);
}

sub readsetsumon {
	my($id)=@_;
	my @array=&readcsv($::setsumon);
	&shuffleArray(\@array, %form);

	foreach my $line(@array) {
		my($_line, $_id, $_val, $_text
			, $_n1, $_v1, $_n2, $_v2, $_n3, $_v3)
			= split(/\t/, $line);
		if($id eq $_id) {
			my %h;
			$_val=~s/\s.*//g;
			$h{line}=$_line;
			$h{id}=$_id;
			$h{val}=$_val;
			$h{text}=$_text;
			$h{n1}=$_n1;
			$h{v1}=$_v1;
			$h{n2}=$_n2;
			$h{v2}=$_v2;
			$h{n3}=$_n3;
			$h{v3}=$_v3;
			return %h;
		}
	}
}

sub result {
	my ($ptn)=@_;
	my @kekka=&readcsv($::kekka);
	my %h;
	foreach my $line(@kekka) {
		my($_line, $_id, $_cate, $_kata, $_kuken, $_ptn
			, $_sr, $_hr, $_r, $_n, $_kei, $_chara, $_neet
			, $_j)
			= split(/\t/, $line);
		if(uc $ptn eq uc $_ptn) {
			my @neet=split(/,/, $_neet);
			my @jigyou=split(/,/, $_j);
			&shuffleArray(\@neet, %form);
			&shuffleArray(\@jigyou, %form);
			$h{line}=$_line;
			$h{id}=$_id;
			$h{cate}=$_cate;
			$h{kata}=$_kata;
			$h{kuken}=$_kuken;
			$h{ptn}=$_ptn;
			$h{sr}=$_sr;
			$h{hr}=$_hr;
			$h{r}=$_r;
			$h{n}=$_n;
			$h{kei}=$_kei;
			$h{chara}=$_chara;
			$h{neet}=shift @neet;
			$h{jigyou}=shift @jigyou;
			return %h;
		}
	}
}

# みんなの属性を見る
sub resultlist {
	my($start, $max)=@_;
	my $today=&udate;
	my $maxdate=10;
	my $_max=$max;

	my @results;
#	for(my $i=$today; $i>=$today - $maxdate; $i--) {
		if(-r "$::dir{result}/result-0.txt") {
			my $buf=&readfile("$::dir{result}/result-0.txt");
			my @tmp=split(/\n/, $buf);
			@tmp=reverse @tmp;
			my $c=$#tmp+2;
			foreach(@tmp) {
				if($max >= 0) {
					$c--;
#					push(@results, "$i\t$c\t$_");
					push(@results, "0\t$c\t$_");
					$max--;
				}
 			}
		}
#	}

	my @r;
	my $cnt=0;
	foreach(@results) {
		my($ud, $uc, $time, $ip, $neetid, $name, $ptn, $zokusei
			, $neet, $jigyou, $query)=split(/\t/, $_);
		# http://d.hatena.ne.jp/perlcodesample/20091105/1246274997
		my ($second, $minute, $hour, $mday, $month, $year)
			= localtime($time + 0 * 3600);

		my $dtstr=sprintf($resource{datetime}
			,$month+1, $mday, $hour, $minute);
#		$name=&query_decode($name);
		if($start <= $cnt) {
			push(@r, "$ud\t$uc\t$dtstr\t$name\t$ptn");
		}
		$cnt++;
#print "Content-type: text/html; charset=utf-8\n\n$start<br>$_max<br>$cnt<hr>";
		if($start + $_max <= $cnt) {
			last;
		}
	}
	@r;
}

sub readcount {
	my (%zzid)=&readzokusei2;

	my %z;
	my %zid;
	$zid{aaaa}="AAAA";
	$zid{bbbb}="BBBB";
	$zid{cccc}="CCCC";
	$zid{total}="total";

#print Dumper %zzid;
	foreach(keys %zzid) {
		my($l, $r)=split(/=/, $zzid{$_});
#print "Content-type: text/html; charset=utf8\n\ntest1 $l=$r<br>\n";
		$zid{$l}=&dbmname($r);
	}

	foreach(keys %zid) {
		my $s=-s "$::dir{count}/@{[$zid{$_}]}.count";
#print "Content-type: text/html; charset=utf8\n\ntest2 $_=$s<br>\n";
		$z{$_}=0 + $s;
	}
	%z;
}

sub writecount {
	my($type, $count, @zokusei)=@_;
	&addbyte("$::dir{count}/total.count");
	&addbyte("$::dir{count}/$type.count");

	for(my $i=0; $i <= $count; $i++) {
		&addbyte("$::dir{count}/@{[&dbmname($zokusei[$i])]}.count");
	}
}

sub readzokusei2 {
	my @array=&readcsv($::zokusei);
	my %i;
	foreach my $line(@array) {
		my($_dmy, $_id, $_val, $_str, $_name)=split(/\t/, $line);
		$i{$_id}="$_name=$_str";
	}
	return(%i);
}

1;
