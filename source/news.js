﻿// JavaScript Document
/*function NewsDisp(num)
{
	var newsMAX = 5;
	for(i=1; i<=newsMAX; i++) if((id = document.getElementById("news"+i))) id.style.display = "none";
	document.getElementById("news"+num).style.display = "block";
}

window.onload = function()
{
	var s = window.location.search;
	if (s && s.length > 1) {
		var ps = s.substr(1).split('&');
		ps.forEach(function(p){
			var kv = p.split('=');
			if (kv[0] == 'q') NewsDisp(unescape(kv[1]));
		});
	}
};*/

function NewsDisp(num) {
	var newsMAX = 9;
	for (i = 1; i <= newsMAX; i++) {
		var id = $("#news"+i);
		if (id) {
			id.css("display", "none");
		}
	}
	$("#news"+ num).css("display", "block");
}

function changeURL(next_url){
	if (window.history && window.history.pushState){// chack support pushState
		console.log(next_url);
		window.history.pushState(null, null, next_url);
	}
}

function getParam(){
	var s = location.search;
	if (s && s.length > 1) {
		var ps = s.substr(1).split('&');
		ps.forEach(function(p) {
			var kv = p.split('=');
			if (kv[0] == 'q')
				NewsDisp(unescape(kv[1]));
		});
	}
	else $("#news6").css("display", "block");
}

$(function(){
	getParam();

	if (window.history && window.history.pushState){// chack support pushState
		$(window).on('popstate', function(jqevent) {
			console.log(location.href);
			getParam();
			var state = jqevent.originalEvent.state; // stateオブジェクト
			return false;
		});
	}

	$(".listpstn").on("click", "a", function(){
		var param = $(this).attr("href");
		var url = location.href;
		var nextUrl = url.replace(/\?.+/, "")+param;
		changeURL(nextUrl);
		NewsDisp(param.replace("?q=", ""));
		return false;
	})
});
